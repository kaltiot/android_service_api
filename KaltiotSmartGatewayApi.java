package com.kaltiot.smartgatewayapi;

import android.os.Messenger;
import android.os.Message;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.util.Log;

public class KaltiotSmartGatewayApi {

    /* Payload types for notification callback and MSG_SEND_PAYLOAD
     * make sure to keep these in sync with PayloadType in mq_structs.h
     */
    public static final int PAYLOAD_BINARY = 1;
    public static final int PAYLOAD_INT = 2;
    public static final int PAYLOAD_STRING = 3;
    public static final int PAYLOAD_PING = 4;
    public static final int PAYLOAD_PONG = 5;

    /* Message.what values defined, used to pass messages from device (Android application) to
     * service (KaltiotSmartGateway) and vice versa: */

    /**
     * Message from device to service to register a device.
     * Message.replyTo must be a Messenger of the device where callbacks should be sent.
     * Message.getData().getString("address") stores the device's address.
     */
    public static final int MSG_REGISTER_CLIENT = 11;

    /**
     * Message from device to service to unregister a device.
     * Message.replyTo must be a Messenger of the device as previously given with MSG_REGISTER_CLIENT.
     * Message.getData().getString("address") stores the device's address.
     */
    public static final int MSG_UNREGISTER_CLIENT = 12;

    /**
     * Message for sending state from service to device.
     * Message.arg1 is the state.
     * Message.arg2 is the error value.
     * Message.getData().getString("address") stores the device's address.
     */
    public static final int MSG_SET_STATE = 13;

    /**
     * Message for sending a notification from service to device or
     * a publish from device to service.
     * Message.arg1 is the payload type.
     * Message.arg2 is the payload integer.
     * Message.getData().getByteArray("data") stores the payload as byte array.
     * Message.getData().getString("address") stores the device's address.
     * Message.getData().getString("id") stores the message ID from service to device.
     * Message.getData().getString("rid_override") stores the sender ID from device to service.
     */
    public static final int MSG_SEND_PAYLOAD = 14;

    /**
     * Message for sending Resource ID from service to device, or request RID from device.
     * Message.getData().getString("data") stores the device's unique RID.
     */
    public static final int MSG_SET_RID = 15;

    /**
     * Message for sending Application ID from service to device.
     * Message.getData().getString("data") stores the service's application ID.
     */
    public static final int MSG_SET_APPID = 16;

    /**
     * Message for sending Sub Organization Secret Code from device to service.
     * Message.getData().getString("data") stores the secret code received from the user.
     */
    public static final int MSG_SET_SUB_ORG = 17;

    /* Name of the Kaltiot Smart Gateway Android Service */
    private final String serviceName = "com.kaltiot.smartgateway.KaltiotSmartGateway";

    /* States of Kaltiot Smart IoT Client, as defined in mq_common_defines.h */
    public static final int STATE_OFFLINE = 0;
    public static final int STATE_DISCONNECTING = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_ONLINE = 3;

    /* Error values of Kaltiot Smart IoT Client, as defined in mq_common_defines.h */
    public static final int ERROR_NO_ERROR = 0;
    public static final int ERROR_CONNECT = 1;
    public static final int ERROR_DISCONNECT = 2;
    public static final int ERROR_CERT_RENEW = 3;
    public static final int ERROR_SOCKET = 4;
    public static final int ERROR_CERT_CSR_DATA = 5;

    /* Members */
    String addr = null;
    String id = null;
    String[] capabilities = null;
    String suborg = null;
    KaltiotSmartGatewayApiCallbacks appCb = null;
    Context context = null;
    String packageName = null;

    /** Messenger for communicating with service. */
    Messenger mService = null;
    /** Flag indicating whether we have called bind on the service. */
    boolean mIsBound = false;

    /**
     * Construct a connection with device information.
     *  @param packagename      Android .apk package name containing KaltiotSmartGateway
     *  @param address          Free name identifying this device
     *  @param customer_id      Unique identifier for this device, will be shown to the console
     *  @param capas            Capability keys
     *  @param cb               Object implementing the callbacks
     *  @param ctx              Android Context for this Android application
     */
    public KaltiotSmartGatewayApi(String packagename, String address, String customer_id,
                                  String[] capas, String sub_org, KaltiotSmartGatewayApiCallbacks cb, Context ctx) {
        packageName = packagename;
        addr = address;
        id = customer_id;
        capabilities = capas;
        suborg = sub_org;
        appCb = cb;
        context = ctx;
    }

    public void logService() {
        //Log.i("KaltiotSmartGatewayApi", "bound="+mIsBound+" service="+mService);
    }

    /**
     * Start service on the background, this will keep the service always running.
     */
    public void startService() {
        Intent i = new Intent();
        i.setClassName(packageName, serviceName);
        context.startService(i);
    }

    /**
     * Stop a started service.
     */
    public void stopService() {
        Intent i = new Intent();
        i.setClassName(packageName, serviceName);
        context.stopService(i);
    }

    /**
     * Establish a two-way connection with the service.
     * We use an explicit class name because we don't want to let other
     * applications replace our component.
     */
    public void doBindService() {
        Intent i = new Intent();
        i.setClassName(packageName, serviceName);
        context.bindService(i, mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
        logService();
    }

    /**
     * Check if we are connected to KaltiotSmartGateway service.
     * @return true if the application is connected to the service. 
     */
    public boolean isServiceConnected() {
        logService();
        if (mService != null) return true;
        return false;
    }

    /**
     * Unbind this device from the service.
     */
    public void doUnbindService() {
        logService();
        if (mIsBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                /* Unregistering the device from service. The IoT device will still
                   remain in the mq_client. */
                try {
                    Message msg = Message.obtain(null, MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    Bundle b = new Bundle();
                    b.putString("address", addr);
                    b.putString("version", "1");
                    b.putString("id", id);
                    b.putStringArray("capas", null);
                    msg.setData(b);
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }

            // Detach our existing connection.
            context.unbindService(mConnection);
            mIsBound = false;
        }
    }

    /**
     * Publish a binary payload from this device.
     * @param payload          Payload to publish
     * @param payload_type     Type of payload: PAYLOAD_STRING, PAYLOAD_BINARY, PAYLOAD_INT, PAYLOAD_PONG
     */
    public void publish(byte[] payload, int payload_type) {
        publish(payload, payload_type, null);
    }

    public void publish(byte[] payload, int payload_type, String rid_override) {
        publish(payload, payload_type, rid_override, null);
    }

    public void publish(byte[] payload, int payload_type, String rid_override, byte[] notification_id) {
        if (mService != null) try {
            Message msg = Message.obtain(null, MSG_SEND_PAYLOAD);
            Bundle b = new Bundle();
            b.putString("address", addr);
            b.putString("rid_override", rid_override);
            b.putByteArray("data", payload);
            b.putByteArray("id", notification_id);
            msg.arg1 = payload_type;
            msg.replyTo = mMessenger;
            msg.setData(b);
            mService.send(msg);
        } catch (RemoteException e) {
            // There is nothing special we need to do if the service has crashed.
        }
    }

    /**
     * Send sub organization secret code from device to service.
     * @param secret          Secret code for sub organization
     */
    public void setSubOrg(String secret) {

        if (mService != null) try {
            suborg = secret;
            Message msg = Message.obtain(null, MSG_SET_SUB_ORG);
            Bundle b = new Bundle();
            b.putString("address", addr);
            b.putString("data", secret);
            msg.replyTo = mMessenger;
            msg.setData(b);
            mService.send(msg);
        } catch (RemoteException e) {
            // There is nothing special we need to do if the service has crashed.
        }
    }

    /**
     * Request for RID from device. Service will respond with SET_RID.
     */
    public void getRid() {

        if (mService != null) try {
            Message msg = Message.obtain(null, MSG_SET_RID);
            Bundle b = new Bundle();
            b.putString("address", addr);
            msg.replyTo = mMessenger;
            msg.setData(b);
            mService.send(msg);
        } catch (RemoteException e) {
            // There is nothing special we need to do if the service has crashed.
        }
    }

    /**
     * Handler of incoming messages from KaltiotSmartGateway, issue the appropriate callback.
     * The application must implement the callback functions to handle these messages.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            logService();
            switch (msg.what) {
                case MSG_SET_STATE:
                    //Log.i(addr, "Sate: "+msg.arg1+":"+msg.arg2);
                    appCb.state_callback(msg.arg1, msg.arg2);
                    break;
                case MSG_SEND_PAYLOAD:
                    byte[] payload = msg.getData().getByteArray("data");
                    String address = msg.getData().getString("address");
                    String msgid = msg.getData().getString("id");
                    //Log.i(addr, "Received "+msg.arg1+": "+payload);
                    appCb.notification_callback(address, payload, msg.arg1, msgid);
                    break;
                case MSG_SET_RID:
                    String rid = msg.getData().getString("data");
                    address = msg.getData().getString("address");
                    //Log.i(addr, "RID: "+rid);
                    appCb.rid_callback(addr, rid);
                    break;
                case MSG_SET_APPID:
                    String appid = msg.getData().getString("data");
                    address = msg.getData().getString("address");
                    //Log.i(addr, "AppID: "+appid);
                    appCb.appid_callback(addr, appid);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Messenger for service to sensor messages.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  We are communicating with our
            // service through an IDL interface, so get a client-side
            // representation of that from the raw service object.
            mService = new Messenger(service);

            //Log.i(addr, "Bound to service, registering.");
            // We want to monitor the service for as long as we are
            // connected to it.
            sendClientRegister();
        }

        public void sendClientRegister() {

            try {
                Message msg = Message.obtain(null, MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                // Address identifies the sensor, this sensor gets the same RID every time
                Bundle b = new Bundle();

                b.putString("address", addr);
                b.putString("version", "1");
                b.putString("id", id);
                b.putString("suborg", suborg);
                b.putStringArray("capas", capabilities);

                msg.setData(b);
                mService.send(msg);

            } catch (RemoteException e) {
                // In this case the service has crashed before we could even
                // do anything with it; we can count on soon being
                // disconnected (and then reconnected if it can be restarted)
                // so there is no need to do anything here.
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
        }
    };
}
