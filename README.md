# README #

* Android Service API for using KaltiotSmartGateway from any Android application
* This API is a function/callback interface that can be used instead of Android Messenger interface

### Adding Kaltiot Smart IoT to your existing Android application ###

All you need to do is download your Android SDK from https://console.torqhub.io and add the library to your project
* copy your Android SDK to YourAndroidApplication/KaltiotSmartGateway/KaltiotSmartGateway.aar
* edit YourAndroidApplication/settings.gradle and make sure it has
```
include ':KaltiotSmartGateway'
```
* edit YourAndroidApplication/YourApp/build.gradle and add following lines in dependencies section:
```
dependencies {
    compile project(':KaltiotSmartGateway')
    compile 'com.android.support:support-v4:22.2.0'
    compile 'com.google.protobuf:protobuf-java:2.6.1'
}
```
* That's it, then your code can use KaltiotSmartGatewayApi

### Sample Code ###

Add the following lines of code into your Android Activity to use the KaltiotSmartGateway API.

```
#!java

import com.kaltiot.smartgatewayapi.KaltiotSmartGatewayApi;
import com.kaltiot.smartgatewayapi.KaltiotSmartGatewayApiCallbacks;

public class YourActivity implements KaltiotSmartGatewayApiCallbacks {

    KaltiotSmartGatewayApi kaltiotSmartGatewayApi;

    public void onCreate() {
        // Set up your device.
        kaltiotSmartGatewayApi = new KaltiotSmartGatewayApi("MyApp", "MyCustomerId", null, this, this);

        // Start service if you want it to run indefinitely on the background
        // and to be able to receive notifications on the background.
        // This is not needed if you only need the connection when your app is bound
        // to the gateway service.
        kaltiotSmartGatewayApi.startService();

        // Bind to Service to register and receive notifications.
        kaltiotSmartGatewayApi.doBindService();
    }

    public void onDestroy() {
        kaltiotSmartGatewayApi.doUnbindService();
    }

    public void state_callback(int state, int error) {
        // This is called whenever the connection state changes.
        // You may update the UI to show the connection status.
    }

    public void rid_callback(String address, String rid) {
        // This is called when your device receives its unique Resource ID.
    }

    public void appid_callback(String address, String appid) {
        // This is called to inform your device what is the gateway's application ID.
        // Messaging is only possible for devices sharing the same app ID.
    }

    public void notification_callback(String address, byte[] payload,
                                      int payload_type, String msg_id) {
        // This is called whenever your device receives a notification.

        // Respond to ping.
        if (payload_type == KaltiotSmartGatewayApi.PAYLOAD_PING && address.equals("MyApp")) {
            String publish = "device_pong";
            kaltiotSmartGatewayApi.publish(publish.getBytes(), KaltiotSmartGatewayApi.PAYLOAD_PONG);
        }
    }
}
```

### Example Application ###

Kaltiot Smart IoT Sample is an example application that uses the KaltiotSmartGateway API. The source codes are available at 
[bitbucket.org/kaltiot/android_sensor.git](https://bitbucket.org/kaltiot/android_sensor.git)